#!/bin/bash

apt-get update && apt-get upgrade -y

apt-get install locales && locale-gen en_US.UTF-8 && export LC_ALL="C"

apt-get install -y gcc cmake pkg-config gcc-mingw-w64 libgnutls28-dev perl-base heimdal-dev libpopt-dev \
                   libglib2.0-dev doxygen libgpgme-dev uuid-dev libssh-gcrypt-dev libldap2-dev libhiredis-dev \
                   libxml2-dev libradcli-dev libpcap-dev bison libksba-dev libsnmp-dev libgcrypt20-dev redis-server \
                   gnutls-bin python-impacket python-netsnmp python3-dev python3-pip libpq-dev postgresql postgresql-contrib \
                   postgresql-server-dev-all libical-dev libmicrohttpd-dev virtualenv xmltoman nmap snmp pnscan dsniff net-tools \
                   wget curl sudo rsync

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - &&
echo 'deb https://dl.yarnpkg.com/debian/ stable main' | sudo tee /etc/apt/sources.list.d/yarn.list &&
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - &&
apt-get update &&
apt-get install -y yarn &&
apt-get install -y nodejs

#echo 'export PATH="$PATH:/opt/gvm/bin:/opt/gvm/sbin:/opt/gvm/.local/bin"' | tee -a /etc/profile.d/gvm.sh &&
#chmod 0755 /etc/profile.d/gvm.sh &&
#source /etc/profile.d/gvm.sh
echo "PATH=\"/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/gvm/bin:/opt/gvm/sbin:/opt/gvm/.local/bin\"" > /etc/environment

bash -c 'cat << EOF > /etc/ld.so.conf.d/gvm.conf
# gmv libs location
/opt/gvm/lib
EOF'

mkdir -p /opt/gvm/src
adduser gvm --disabled-password --home /opt/gvm/ --no-create-home --gecos '' &&
usermod -aG redis gvm &&
chown -R gvm:gvm /opt/gvm/

echo 'Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/opt/gvm/sbin"' >> /etc/sudoers &&
echo 'gvm ALL = NOPASSWD: /opt/gvm/sbin/openvas' >> /etc/sudoers &&
echo 'gvm ALL = NOPASSWD: /opt/gvm/sbin/gsad' >> /etc/sudoers
