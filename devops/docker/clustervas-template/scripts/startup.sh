#!/bin/bash

RD='\033[0;31m'
LGR='\033[1;32m'
GR='\033[0;32m'
NC='\033[0m'

WORKDIR="/opt/gvm/src"

# Change directory
cd ${WORKDIR}

# Redis compliance checks
echo -e "${LGR}Redis socket kontrol ediliyor...${NC}"
if [ ! -d "/run/redis" ]; then
	mkdir /run/redis
fi

if  [ -S /run/redis/redis.sock ]; then
        rm /run/redis/redis.sock
fi

# Start Redis server
echo -e "${LGR}Redis server baslatiliyor...${NC}"
service redis-server start

# Check if redis.sock is created
while  [ ! -S /run/redis/redis.sock ]; do
        sleep 1
done

# Check if redis-server is ready
until  [ "${redis-cli -s /run/redis/redis.sock ping}" != "PONG" ]; do
        sleep 1
done
echo -e "${LGR}Redis hazir.${NC}"

# Updating NVT info into redis store from NVT files
echo -e "${LGR}NVT bilgilerini Redis'e aktariyor...${NC}"
su -c "openvas -u" gvm

# Create DB folder
echo -e "${LGR}PostgreSQL DB olusturuluyor...${NC}"
mkdir -p /opt/gvm/data/databases
chown -R postgres:postgres /opt/gvm/data/
su -c "/usr/lib/postgresql/10/bin/initdb /opt/gvm/data/database" postgres
su -c "/usr/lib/postgresql/10/bin/pg_ctl -D /opt/gvm/data/database -l /opt/gvm/data/log.txt start" postgres

# Create GVM DB
echo -e "${LGR}GVM DB olusturuluyor...${NC}"
su -c "createuser -DRS gvm" postgres
su -c "createdb -O gvm gvmd" postgres
su -c "psql --dbname=gvmd --command='create role dba with superuser noinherit;'" postgres
su -c "psql --dbname=gvmd --command='grant dba to gvm;'" postgres
su -c "psql --dbname=gvmd --command='create extension \"uuid-ossp\";'" postgres
su -c "psql --dbname=gvmd --command='create extension \"pgcrypto\";'" postgres

# Generate certificates
echo -e "${LGR}GVM sertifikalari olusturuluyor...${NC}"
su -c "/opt/gvm/bin/gvm-manage-certs -a" gvm

# Create admin user
echo -e "${LGR}GVM kullanicisi olusturuluyor...${NC}"
su -c "/opt/gvm/sbin/gvmd --create-user=admin --password=admin" gvm

# Feed Import Owner setup
echo -e "${LGR}Feed Import Owner ayarlari yapiliyor...${NC}"
USERID=$(su -c "/opt/gvm/sbin/gvmd --get-users --verbose | cut -d \" \" -f2" gvm)
echo -e "\t${GR}Kullanici ID: ${USERID}${NC}"
su -c "/opt/gvm/sbin/gvmd --modify-setting 78eceaec-3385-11ea-b237-28d24461215b --value ${USERID}" gvm

# Setting up ospd-scanner & gvm-tools
echo -e "${LGR}ospd-Scanner ve gvm-tools yukleniyor...${NC}"
pip3 install ospd ospd-openvas gvm-tools --trusted-host pypi.python.org

echo -e "${LGR}ospd-openvas baslatiliyor...${NC}"
su -c "ospd-openvas --pid-file /opt/gvm/var/run/ospd-openvas.pid --unix-socket=/opt/gvm/var/run/ospd.sock --log-file /opt/gvm/var/log/gvm/ospd-scanner.log --lock-file-dir /opt/gvm/var/run/ospd/" gvm

echo -e "${LGR}GVMD baslatiliyor...${NC}"
echo -e "\t${GR}Verilerin yuklenmesi uzun surebilir [daemonize=yes]{NC}"
su -c "gvmd --osp-vt-update=/opt/gvm/var/run/ospd.sock" gvm

#echo -e "${LGR}GSA arayuzu baslatiliyor...${NC}"
#gsad --drop-privileges=gvm --verbose --http-only --no-redirect --mlisten=127.0.0.1 --mport=9390 --port=9392

sleep 5

echo -e "${LGR}Scanner ayarlaniyor...${NC}"
SCID=$(su -c "/opt/gvm/sbin/gvmd --get-scanners | grep OpenVAS | cut -d \" \" -f1" gvm)
echo -e "\t${GR}Scanner ID: ${SCID}${NC}"
su -c "/opt/gvm/sbin/gvmd --modify-scanner=${SCID} --scanner-host=/opt/gvm/var/run/ospd.sock" gvm
